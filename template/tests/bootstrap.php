<?php

// Register Composer autoloader to be used by each test so we don't have to worry about it
require __DIR__.'/../vendor/autoload.php';

// Set the default timezone to be used by each PHP date and date time function
date_default_timezone_set('UTC');