const { series } = require('gulp');

function clean(cb) {
    cb();
}

function build(cb) {
    cb();
}

function help(cb) {
    cb();
}

exports.clean = clean;
exports.build = build;
exports.compile = series(clean, build);
exports.default = help;