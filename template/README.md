# Project Title (v.0.1.0)
Project description

## Getting Started
**Tested on Debian 10 Buster with Docker v19.03.4**

These instructions should get you a copy of the project up and running for development and testing purposes. Please refer to the deployment section for how to deploy the project on a live system.

### Prerequisites
The following software, tools, binaries, etc will be required to get the project up and running

* Docker v
* Node.js
* NPM

### Setup
These step by step instructions should get you a development environment running

1. ```Command to run``` Placeholder annotation
2. ```Command to run``` Placeholder annotation
3. ```Command to run``` Placeholder annotation


### Usage
Placeholder text for usage example

``` Placeholder for usage example ```

## Testing

### Unit tests
Unit tests are done with PHPUnit, execute the following in the root folder of the project to run the test suite:

``` phpunit ```

### Feature tests
Feature tests are done with Behat, execute the following in the root folder of the project to run the test suite:

``` behat ```

## Deployment
These step by step instructions should get you a live deployment in a production environment

1. ```Command to run``` Placeholder annotation
2. ```Command to run``` Placeholder annotation
3. ```Command to run``` Placeholder annotation


## Built With
* [Placeholder](#) - Placeholder
* [Placeholder](#) - Placeholder
* [Placeholder](#) - Placeholder

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change. Please make sure to update tests as appropriate.

## Versioning
[Semantic Versioning 2.0.0](https://semver.org/spec/v2.0.0.html)

## Authors
* [Alex Paphitis](https://paphitis.net)

## License
[MIT License](LICENSE)

## Acknowledgments
* [Placeholder](#Acknowledgments) - Placeholder
* [Placeholder](#Acknowledgments) - Placeholder
* [Placeholder](#Acknowledgments) - Placeholder